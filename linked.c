#include<stdio.h>
#include<stdlib.h>

#define bool int
#define true 1
#define false 0

// we might want to add a pointer to the previous Item


typedef struct Item
{
    int value;
    char* word;
    struct Item* next;
}_Item;

typedef struct List
{
    struct Item* item;
    int length;
}_List;

struct List initList()
{
    struct List list;
    struct Item* head = malloc(sizeof(struct Item));
    head -> next = NULL;
    list.item = head;
    return list;
}


struct Item* gethead(struct Item list)
{
    struct Item* point = list.next;
    while(point != NULL)
    {
        point = point -> next;
    }
    return point;
}

void appendItem(struct List* list, int value)
{
    struct Item* head = list -> item;
    if(head -> next != NULL)
    {
        head = head -> next;
    }
    head -> next = malloc(sizeof(struct Item));
    head = head -> next;
    head -> next = NULL;
    head -> value = value;
}


int main()
{
    struct List list = initList();
    appendItem(&list,1);
    appendItem(&list,2);
    appendItem(&list,3);
    appendItem(&list,4);
    struct Item* item = gethead(&list);
    printf("Item value: %d\n",item -> value);
    return 0;
}
